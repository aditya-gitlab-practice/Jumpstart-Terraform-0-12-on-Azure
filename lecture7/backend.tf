terraform {
  backend "azurerm" {
    resource_group_name  = "AzureDBricks"
    storage_account_name = "azuredbob1289"
    container_name       = "azuredbcontainer"
    key                  = "prod.terraform.tfstate"
  }
}
